const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving clients...');

  let clientsMoved = 0;
  let lastClientId;
  const clients = await mssqlConnection.query(`
    SELECT *, (SELECT COUNT(*) FROM contacts WHERE contacts.contact_id = clients.bill_contact) billingContactExists FROM clients ORDER BY Client_ID ASC
    `);
  const clientsToInsert = [];

  for (let clientIndex = 0; clientIndex < clients.recordset.length; clientIndex++) {
    const client = clients.recordset[clientIndex];

    const dataToInsert = {
      id: client.Client_ID,
      name: client.Client_name.trim(),
      extra_id: client.Client_ID1.trim(),
      extra_id2: (client.Client_ID2 || '').trim(),
      type: client.Client_type.trim(),
      contact_address1: (client.Client_Address1 || '').trim(),
      contact_address2: (client.Client_Address2 || '').trim(),
      contact_address3: (client.Client_Address3 || '').trim(),
      contact_city: (client.Client_City || '').trim(),
      contact_state: (client.Client_State || '').trim(),
      contact_zip: (client.Client_Zip || '').trim(),
      contact_country: client.client_country.trim(),
      contact_phone: (client.Client_Phone1 || '').trim(),
      contact_website: (client.client_www || '').trim(),
      contact_fax: (client.bill_fax || '').trim(),
      contact_email: (client.bill_email || '').trim(),
      billing_contact_id: !!client.billingContactExists ? client.bill_contact : null,
      archived_at: client.Status !== 'A' ? client.date_modified : null,
      project_manager_id: client.client_manager,
      invoice_method: client.invoice_method,
      net_days: client.net_days,
      expense_markup: client.expense_markup,
      deadline_date: (client.Date_complete || '').trim(),
      review_date: (client.date_review || '').trim(),
      company_id: client.Company_Id,
      created_at: client.date_added,
      updated_at: client.date_modified,
    };

    lastClientId = dataToInsert.id;

    clientsToInsert.push(Object.values(dataToInsert));

    if (clientsToInsert.length >= 2000 || clientIndex === clients.recordset.length - 1) {
      const bulkParams = generateBulkInsert('clients', Object.keys(dataToInsert), clientsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      clientsMoved += clientsToInsert.length;
      clientsToInsert.length = 0;

      console.log(clientsMoved + ' clients moved');
    }
  }

  async function afterCommit() {
    await pgConnection.query('ALTER TABLE clients ALTER COLUMN id RESTART WITH ' + (lastClientId + 1));
  }

  return { afterCommit };
};