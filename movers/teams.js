const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function moveUsers(mssqlConnection, pgConnection) {
  console.log('Moving teams...');

  let groupsMoved = 0;
  const groups = await mssqlConnection.query('select * from timekeeper_groups order by id asc');
  const groupsToInsert = [];
  let lastGroupId;

  for (let groupIndex = 0; groupIndex < groups.recordset.length; groupIndex++) {
    const group = groups.recordset[groupIndex];

    const dataToInsert = {
      id: group.id,
      company_id: group.company_id,
      name: group.timekeeper_group.trim(),
    };

    lastGroupId = dataToInsert.id;

    groupsToInsert.push(Object.values(dataToInsert));

    if (groupsToInsert.length >= 1000 || groupIndex === groups.recordset.length - 1) {
      const bulkParams = generateBulkInsert('teams', Object.keys(dataToInsert), groupsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      groupsMoved += groupsToInsert.length;
      groupsToInsert.length = 0;

      console.log(groupsMoved + ' teams moved');
    }
  }

  await pgConnection.query('ALTER TABLE teams ALTER COLUMN id RESTART WITH ' + (lastGroupId + 1));
};