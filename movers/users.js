const generateBulkInsert = require('../helpers/generate-bulk-insert');
const hashPassword = require('../helpers/hash-password');

const roles = {
  0: 'timekeeper',
  1: 'data_entry',
  2: 'admin',
  3: 'report_viewer',
  4: 'manager',
  5: 'manager', // lazy fix. I don't know what it is for
  6: 'manager', // lazy fix. I don't know what it is for
};

module.exports = async function moveUsers(mssqlConnection, pgConnection) {
  console.log('Moving users...');

  let usersMoved = 0;
  const users = await mssqlConnection.query('select * from users ORDER BY Timekeeper_ID ASC');
  const usersToInsert = [];
  let lastUserId;

  for (let userIndex = 0; userIndex < users.recordset.length; userIndex++) {
    const user = users.recordset[userIndex];

    const dataToInsert = {
      id: user.Timekeeper_ID,
      company_id: user.Company_Id,
      login: user.login.trim(),
      email: user.Office_email.trim(),
      first_name: user.First_Name.trim(),
      middle_name: user.Middle_Initial.trim(),
      last_name: user.Last_Name.trim(),
      password: await hashPassword(user.Password.trim(), 1),
      //  password: '',
      created_at: user.date_added,
      contact_address: (user.Timekeeper_Address && user.Timekeeper_Address.trim().length > 0) ? user.Timekeeper_Address.trim() : '',
      contact_apartment: (user.Timekeeper_unit && user.Timekeeper_unit.trim().length > 0) ? user.Timekeeper_unit.trim() : '',
      contact_city: (user.Timekeeper_city && user.Timekeeper_city.trim().length > 0) ? user.Timekeeper_city.trim() : '',
      contact_state: (user.Timekeeper_state && user.Timekeeper_state.trim().length > 0) ? user.Timekeeper_state.trim() : '',
      contact_zip: (user.Timekeeper_zip && user.Timekeeper_zip.trim().length > 0) ? user.Timekeeper_zip.trim() : '',
      contact_country: (user.Timekeeper_country && user.Timekeeper_country.trim().length > 0) ? user.Timekeeper_country.trim() : '',
      contact_email: (user.Timekeeper_email && user.Timekeeper_email.trim().length > 0) ? user.Timekeeper_email.trim() : '',
      custom_id: (user.Timekeeper_ID1 && user.Timekeeper_ID1.trim().length > 0) ? user.Timekeeper_ID1.trim() : '',
      export_id: (user.Timekeeper_ID2 && user.Timekeeper_ID2.trim().length > 0) ? user.Timekeeper_ID2.trim() : '',
      default_activity_id: user.user_default_activity_id,
      work_title: (user.Title && user.Title.trim().length > 0) ? user.Title.trim() : '',
      work_position: (user.position && user.position.trim().length > 0) ? user.position.trim() : '',
      active: user.Status === 'A',
      notes: (user.notes && user.notes.trim().length > 0) ? user.notes.trim() : '',
      role: roles[user.Permission_Level],
    };

    if (dataToInsert.email === null && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(dataToInsert.login)) {
      dataToInsert.email = dataToInsert.login;
    }

    if (dataToInsert.work_title.length > 0 && dataToInsert.work_position.length === 0 || (dataToInsert.work_title.length > 0 && dataToInsert.work_position.length > 0 && dataToInsert.work_title.toLowerCase() === dataToInsert.work_position.toLowerCase())) {
      dataToInsert.work_title = '';
      dataToInsert.work_position = dataToInsert.work_title;
    }

    lastUserId = dataToInsert.id;

    usersToInsert.push(Object.values(dataToInsert));

    if (usersToInsert.length >= 1000 || userIndex === users.recordset.length - 1) {
      const bulkParams = generateBulkInsert('users', Object.keys(dataToInsert), usersToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      usersMoved += usersToInsert.length;
      usersToInsert.length = 0;

      console.log(usersMoved + ' users moved');
    }
  }

  async function afterCommit() {
    await pgConnection.query('ALTER TABLE users ALTER COLUMN id RESTART WITH ' + (lastUserId + 1));
  }

  return { afterCommit };
};