const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving project categories...');

  let lastCategoryId;
  let categoriesMoved = 0;
  const categories = await mssqlConnection.query('SELECT * FROM project_category ORDER BY project_category_id ASC');
  const categoriesToInsert = [];

  for (let categoryIndex = 0; categoryIndex < categories.recordset.length; categoryIndex++) {
    const category = categories.recordset[categoryIndex];

    const dataToInsert = {
      id: category.project_category_id,
      name: category.description.trim(),
      color: (category.color || '').trim().length > 0 ? category.color.trim() : null,
      company_id: category.company_id,
      created_at: category.insert_date,
    };

    lastCategoryId = dataToInsert.id;

    categoriesToInsert.push(Object.values(dataToInsert));

    if (categoriesToInsert.length >= 5000 || categoryIndex === categories.recordset.length - 1) {
      const bulkParams = generateBulkInsert('project_categories', Object.keys(dataToInsert), categoriesToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      categoriesMoved += categoriesToInsert.length;
      categoriesToInsert.length = 0;

      console.log(categoriesMoved + ' project categories moved');
    }
  }

  await pgConnection.query('ALTER TABLE project_categories ALTER COLUMN id RESTART WITH ' + (lastCategoryId + 1));
};