const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving projects...');

  let lastProjectId;
  let projectsMoved = 0;
  const projects = await mssqlConnection.query(`
    SELECT *, (SELECT COUNT(*) 
        FROM project_category 
        WHERE project_category.project_category_id = projects.project_category_id) project_category_exists,
        (SELECT COUNT(*) 
        FROM users 
        WHERE users.timekeeper_id = projects.person_responsible) person_responsible_exists,
        (SELECT COUNT(*) 
        FROM contacts 
        INNER JOIN clients ON clients.client_id = contacts.client_id
        WHERE contacts.contact_ID = projects.contact_id) contact_exists 
    FROM Projects  
    WHERE 
    (SELECT COUNT(*) FROM Clients WHERE Projects.Client_id = Clients.Client_Id) > 0  
    ORDER BY Project_Id ASC`);
  const projectsToInsert = [];

  for (let projectIndex = 0; projectIndex < projects.recordset.length; projectIndex++) {
    const project = projects.recordset[projectIndex];

    const dataToInsert = {
      id: project.Project_Id,
      name: project.Project_Name.trim(),
      extra_id: project.project_code,
      archived_at: project.status === 'N' ? new Date() : null,
      extra_id2: project.project_importcode,
      type: project.project_type,
      percent_complete: !isNaN(parseFloat(project.percent_complete)) ? project.percent_complete : null,
      contact_id: !!project.contact_exists ? project.contact_id : null,
      use_client_project_manager: project.project_manager === 0,
      manager_id: project.project_manager === 0 ? null : project.project_manager,
      responsible_user_id: !!project.person_responsible_exists ? project.person_responsible : null,
      default_activity_id: project.default_activityid,
      assigned_at: project.date_assigned,
      deadline_at: project.date_complete,
      default_time_entry_billability: project.Billable,
      expense_markup: project.expense_markup === 0 ? null : project.expense_markup,
      contact_address1: (project.address1 || '').trim(),
      contact_address2: (project.address2 || '').trim(),
      contact_address3: (project.address3 || '').trim(),
      contact_city: (project.city || '').trim(),
      contact_county: (project.county || '').trim(),
      contact_state: (project.state || '').trim(),
      contact_zip: (project.zip || '').trim(),
      contact_country: (project.country || '').trim(),
      contact_notes: (project.notes || '').trim(),
      client_id: project.Client_Id,
      category_id: !!project.project_category_exists ? project.project_category_id : null,
      parent_id: (project.Project_Id !== project.project_parent && parseInt(project.project_parent, 10)) || null,
      company_id: project.Company_Id,
      created_at: project.date_added,
    };

    dataToInsert.nesting_id = dataToInsert.parent_id || dataToInsert.id;
    dataToInsert.nesting_level = dataToInsert.parent_id ? 2 : 1;

    lastProjectId = dataToInsert.id;

    projectsToInsert.push(Object.values(dataToInsert));

    if (projectsToInsert.length >= 1000 || projectIndex === projects.recordset.length - 1) {
      const bulkParams = generateBulkInsert('projects', Object.keys(dataToInsert), projectsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      projectsMoved += projectsToInsert.length;
      projectsToInsert.length = 0;

      console.log(projectsMoved + ' projects moved');
    }
  }

  await pgConnection.query('ALTER SEQUENCE projects_id_seq RESTART WITH ' + (lastProjectId + 1));
};