const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving client contacts...');

  let contactsMoved = 0;
  let lastContactId;
  const contacts = await mssqlConnection.query(`
    SELECT * 
    FROM contacts 
    INNER JOIN clients ON clients.client_id = contacts.client_id
    ORDER BY contact_ID ASC
    `);
  const contactsToInsert = [];

  for (let contactIndex = 0; contactIndex < contacts.recordset.length; contactIndex++) {
    const contact = contacts.recordset[contactIndex];

    const dataToInsert = {
      id: contact.contact_ID,
      extra_id: contact.contact_extid.trim(),
      first_name: contact.contact_firstname.trim(),
      middle_name: '',
      last_name: contact.contact_lastname.trim(),
      address1: contact.contact_address1.trim(),
      address2: contact.contact_address2.trim(),
      city: contact.contact_city.trim(),
      state: contact.contact_state.trim(),
      zip: contact.contact_zip.trim(),
      country: contact.contact_country.trim(),
      email: contact.contact_email.trim(),
      phone: contact.contact_phone.trim(),
      notes: contact.notes.trim(),
      client_id: contact.client_id,
    };

    lastContactId = dataToInsert.id;

    contactsToInsert.push(Object.values(dataToInsert));

    if (contactsToInsert.length >= 2000 || contactIndex === contacts.recordset.length - 1) {
      const bulkParams = generateBulkInsert('client_contacts', Object.keys(dataToInsert), contactsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      contactsMoved += contactsToInsert.length;
      contactsToInsert.length = 0;

      console.log(contactsMoved + ' clients contacts moved');
    }
  }

  async function afterCommit() {
    await pgConnection.query('ALTER TABLE client_contacts ALTER COLUMN id RESTART WITH ' + (lastContactId + 1));
  }

  return { afterCommit };
};