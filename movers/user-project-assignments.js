const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection, { user_id }) {
  console.log('Moving user-project assignments...');

  let refsMoved = 0;
  let lastProjectId = 0;
  let refs;

  do {
    refs = await mssqlConnection.query(`
        SELECT *, (SELECT date_added FROM tkproj c WHERE c.project_id = b.project_id AND c.timekeeper_id = b.timekeeper_id) date_added  FROM (
        SELECT DISTINCT a.project_id, a.timekeeper_id FROM tkproj a
        WHERE a.project_id IN (
            SELECT TOP 10000 d.project_id 
            FROM projects d
            WHERE project_id > ${lastProjectId}
            AND (SELECT COUNT(*) FROM clients e WHERE d.client_id = e.client_id) > 0
            ${user_id === undefined ? '' : `AND d.project_id IN (SELECT e.project_id FROM tkproj e WHERE e.timekeeper_id =  ${user_id})`}
            ORDER BY d.project_id ASC
        )
        AND (SELECT COUNT(*) FROM users f WHERE f.timekeeper_id = a.timekeeper_id) > 0
        ) b ORDER BY b.project_id ASC`);

    const refsToInsert = [];

    for (let refIndex = 0; refIndex < refs.recordset.length; refIndex++) {
      const ref = refs.recordset[refIndex];

      const dataToInsert = {
        user_id: ref.timekeeper_id,
        project_id: ref.project_id,
        created_at: ref.date_added,
      };

      lastProjectId = dataToInsert.project_id;

      refsToInsert.push(Object.values(dataToInsert));

      if (refsToInsert.length >= 21000 || refIndex === refs.recordset.length - 1) {

        const bulkParams = generateBulkInsert('user_project_assignments', Object.keys(dataToInsert), refsToInsert);
        await pgConnection.query(bulkParams.sql, bulkParams.params);

        refsMoved += refsToInsert.length;
        refsToInsert.length = 0;

        console.log(refsMoved + ' user-project assignments moved');
      }
    }
  } while (refs.recordset.length > 0);
};