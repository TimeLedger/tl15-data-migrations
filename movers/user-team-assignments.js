const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving user-team assignments...');

  let refsMoved = 0;
  const refs = await mssqlConnection.query(`
SELECT r.timekeeper_id, r.group_id, MAX(r.is_manager) is_manager FROM (
    (
      SELECT r.timekeeper_id, r.group_id, 0 as is_manager
  FROM rv_group r
)
  UNION
  (
    SELECT u.timekeeper_id, u.timekeeper_group, u.group_manager
  FROM Users u
)
) r
  WHERE (SELECT COUNT(*) FROM Users u WHERE u.Timekeeper_ID = r.timekeeper_id) > 0
  AND (SELECT COUNT(*) FROM Timekeeper_groups tg WHERE tg.id = r.group_id) > 0
  GROUP BY r.timekeeper_id , r.group_id
    `);

    const refsToInsert = [];

    for (let refIndex = 0; refIndex < refs.recordset.length; refIndex++) {
      const ref = refs.recordset[refIndex];

      const dataToInsert = {
        user_id: ref.timekeeper_id,
        team_id: ref.group_id,
        role: !!ref.is_manager ? 'manager' : 'member',
      };

      refsToInsert.push(Object.values(dataToInsert));

      if (refsToInsert.length >= 10000 || refIndex === refs.recordset.length - 1) {

        const bulkParams = generateBulkInsert('user_team_assignments', Object.keys(dataToInsert), refsToInsert);
        await pgConnection.query(bulkParams.sql, bulkParams.params);

        refsMoved += refsToInsert.length;
        refsToInsert.length = 0;

        console.log(refsMoved + ' user-team assignments moved');
      }
    }
};