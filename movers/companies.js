const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving companies...');

  let companiesMoved = 0;
  let lastCompanyId;
  const companies = await mssqlConnection.query(`
    SELECT 
    c.*, t.*,
    t.timekeeper_groups as timekeeper_groups_term,
    (SELECT MIN(Timekeeper_ID) FROM Users u WHERE u.Permission_Level = 2 AND u.Company_Id = c.Company_Id) creator_id
    FROM Companies c
    LEFT JOIN terms t ON t.company_id = c.company_id
    ORDER BY c.company_id ASC
  `);
  const companiesToInsert = [];

  for (let companyIndex = 0; companyIndex < companies.recordset.length; companyIndex++) {
    const company = companies.recordset[companyIndex];

    const days = { 1: 6, 2: 0, 7: 5 };

    const dataToInsert = {
      id: company.Company_Id,
      name: company.Company_Name.trim(),
      time_zone_id: 1,
      first_week_day: days[company.week_starts_on] || 0,
      client_term: (company.ClientTerm || '').trim().length > 0 ? company.ClientTerm.trim() : 'Client',
      client_id2_term: (company.clientid2 || '').trim().length > 0 ? company.clientid2.trim() : 'Second Client ID',
      clients_term: (company.clients || '').trim().length > 0 ? company.clients.trim() : 'Clients',
      user_term: (company.TimekeeperTerm || '').trim().length > 0 ? company.TimekeeperTerm.trim() : 'User',
      user_id2_term: (company.userid2 || '').trim().length > 0 ? company.userid2.trim() : 'Second User ID',
      users_term: (company.Timekeepers || '').trim().length > 0 ? company.Timekeepers.trim() : 'Users',
      group_term: (company.timekeeper_group || '').trim().length > 0 ? company.timekeeper_group.trim() : 'Group',
      groups_term: (company.timekeeper_groups_term || '').trim().length > 0 ? company.timekeeper_groups_term.trim() : 'Groups',
      project_term: (company.ProjectTerm || '').trim().length > 0 ? company.ProjectTerm.trim() : 'Project',
      project_manager_term: (company.salesperson || '').trim().length > 0 ? company.salesperson.trim() : 'Manager',
      project_id2_term: (company.projectid2 || '').trim().length > 0 ? company.projectid2.trim() : 'Second Project ID',
      project_export_code_term: (company.export_code || '').trim().length > 0 ? company.export_code.trim() : 'Code for exporting',
      projects_term: (company.Projects || '').trim().length > 0 ? company.Projects.trim() : 'Projects',
      subproject_term: (company.SubprojectTerm || '').trim().length > 0 ? company.SubprojectTerm.trim() : 'Subproject',
      subprojects_term: (company.Subprojects || '').trim().length > 0 ? company.Subprojects.trim() : 'Subprojects',
      activity_term: (company.activity_singular || '').trim().length > 0 ? company.activity_singular.trim() : 'Activity',
      activity_id2_term: (company.activityid2 || '').trim().length > 0 ? company.activityid2.trim() : 'Second Activity ID',
      activities_term: (company.activity_plural || '').trim().length > 0 ? company.activity_plural.trim() : 'Activities',
      billable_term: (company.billable || '').trim().length > 0 ? company.billable.trim() : 'Billable',
      nonbillable_term: (company.nonbillable || '').trim().length > 0 ? company.nonbillable.trim() : 'Non-Billable',
      rate_term: (company.rate || '').trim().length > 0 ? company.rate.trim() : 'Rate',
      timesheet_term: (company.timesheet || '').trim().length > 0 ? company.timesheet.trim() : 'Timesheet',
      time_entry_tag_term: (company.entrytag || '').trim().length > 0 ? company.entrytag.trim() : 'Time Entry Tag',
      time_entry_time_step: company.billing_increment || 5,
      time_entry_activity_mutability: !!company.show_activity ? 2 : 1,
      time_entry_billability_mutability: +!!company.show_billable,
      require_time_entry_description: !!company.description_required,
      creator_id: company.creator_id,
      created_at: company.date_added,
    };

    lastCompanyId = dataToInsert.id;

    companiesToInsert.push(Object.values(dataToInsert));

    if (companiesToInsert.length >= 1000 || companyIndex === companies.recordset.length - 1) {
      const bulkParams = generateBulkInsert('companies', Object.keys(dataToInsert), companiesToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      companiesMoved += companiesToInsert.length;
      companiesToInsert.length = 0;

      console.log(companiesMoved + ' companies moved');
    }
  }

  async function afterCommit() {
    await pgConnection.query('ALTER TABLE companies ALTER COLUMN id RESTART WITH ' + (lastCompanyId + 1));
  }

  return { afterCommit };
};