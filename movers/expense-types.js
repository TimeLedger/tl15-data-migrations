const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving expense types...');

  let expenseTypesMoved = 0;
  let lastExpenseTypeId;
  const expenseTypes = await mssqlConnection.query('SELECT * FROM ExpenseType ORDER BY expenseTypeId ASC');
  const expenseTypesToInsert = [];

  for (let expenseTypeIndex = 0; expenseTypeIndex < expenseTypes.recordset.length; expenseTypeIndex++) {
    const expenseType = expenseTypes.recordset[expenseTypeIndex];

    const dataToInsert = {
      id: expenseType.expenseTypeID,
      markup: expenseType.expense_markup || 0,
      rate: expenseType.rate,
      rated: expenseType.rated,
      term_unit: expenseType.singular_unit,
      term_units: expenseType.units,
      name: expenseType.typeName.trim(),
      company_id: expenseType.companyID,
    };

    lastExpenseTypeId = dataToInsert.id;

    expenseTypesToInsert.push(Object.values(dataToInsert));

    if (expenseTypesToInsert.length >= 8000 || expenseTypeIndex === expenseTypes.recordset.length - 1) {
      const bulkParams = generateBulkInsert('expense_types', Object.keys(dataToInsert), expenseTypesToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      expenseTypesMoved += expenseTypesToInsert.length;
      expenseTypesToInsert.length = 0;

      console.log(expenseTypesMoved + ' expense types moved');
    }
  }

  await pgConnection.query('ALTER TABLE expense_types ALTER COLUMN id RESTART WITH ' + (lastExpenseTypeId + 1));
};