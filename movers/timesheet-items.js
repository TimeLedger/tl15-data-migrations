const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving timesheet items...');

  let itemsMoved = 0;
  const itemsToInsert = [];

  const items = await mssqlConnection.query(`
    SELECT * 
    FROM tk_proj_act_billable ti
    INNER JOIN projects p ON p.project_id = ti.project_id AND (SELECT COUNT(*) FROM clients c WHERE p.client_id = c.client_id) > 0
    INNER JOIN activities a ON a.activity_id = ti.activity_id
  `);

  for (let itemIndex = 0; itemIndex < items.recordset.length; itemIndex++) {
    const item = items.recordset[itemIndex];

    const dataToInsert = {
      user_id: item.timekeeper_id,
      project_id: item.project_id,
      activity_id: item.activity_id === 1 ? null : item.activity_id,
      billable: item.billable,
      created_at: item.date_used,
    };


    itemsToInsert.push(Object.values(dataToInsert));

    if (itemsToInsert.length >= 5000 || itemIndex === items.recordset.length - 1) {
      const bulkParams = generateBulkInsert('timesheet_items', Object.keys(dataToInsert), itemsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      itemsMoved += itemsToInsert.length;
      itemsToInsert.length = 0;

      console.log(itemsMoved + ' timesheet items moved');
    }
  }
};