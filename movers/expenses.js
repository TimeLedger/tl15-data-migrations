const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection, { user_id }) {
  console.log('Moving expenses...');

  let expensesMoved = 0;
  let lastExpenseId = 0;
  let expenses;

  do {
    expenses = await mssqlConnection.query(`
        SELECT TOP 200000 
        Expenses.*, 
        CAST(items AS DECIMAL(19, 3)) items_fixed,
        CAST(rate AS DECIMAL(19, 3)) rate_fixed,
        IIF(items != 0, ROUND(CAST(items AS DECIMAL(19, 3)) * CAST(rate AS DECIMAL(19, 3)), 4), total) cost, 
        ROUND((IIF(items != 0, ROUND(CAST(items AS DECIMAL(19, 3)) * CAST(rate AS DECIMAL(19, 3)), 4), total) * (1 + markup / 100)), 4) new_total, 
        (SELECT COUNT(*) FROM expensePacket WHERE expensePacket.packetID = Expenses.PacketID) packetExists FROM Expenses 
        WHERE 
        expense_id > ${lastExpenseId}
        AND items < 99999997952
        AND (
            SELECT COUNT(*) FROM Projects 
            WHERE Projects.Project_Id = Expenses.Project_id AND
            (SELECT COUNT(*) FROM Clients WHERE Projects.Client_id = Clients.Client_Id) > 0
        ) > 0
        AND (
            SELECT COUNT(*) FROM ExpenseType 
            WHERE ExpenseType.expenseTypeID = Expenses.expensetype_id 
        ) > 0
        ${user_id === undefined ? '' : `AND timekeeper_id = ${user_id}`}
        ORDER BY expense_id ASC
        `);

    const expensesToInsert = [];

    for (let expenseIndex = 0; expenseIndex < expenses.recordset.length; expenseIndex++) {
      const expense = expenses.recordset[expenseIndex];

      const dataToInsert = {
        id: expense.expense_id,
        project_id: expense.project_id,
        user_id: expense.timekeeper_id,
        date: expense.date_exp,
        created_at: expense.date_entered,
        description: expense.description.trim(),
        note: (expense.explanation || '').trim(),
        total: expense.new_total,
        cost: expense.cost,
        rate: (expense.items_fixed && expense.rate_fixed) || null,
        items: (expense.rate_fixed && expense.items_fixed) || null,
        markup: expense.markup || 0,
        reimbursable: expense.Reimbursable,
        billable: expense.billable,
        tag_id: !!expense.packetExists ? expense.PacketID : null,
        type_id: expense.expensetype_id,
      };

      lastExpenseId = dataToInsert.id;
      expensesToInsert.push(Object.values(dataToInsert));

      if (expensesToInsert.length >= 4000 || expenseIndex === expenses.recordset.length - 1) {
        const bulkParams = generateBulkInsert('expenses', Object.keys(dataToInsert), expensesToInsert);
        await pgConnection.query(bulkParams.sql, bulkParams.params);

        expensesMoved += expensesToInsert.length;
        expensesToInsert.length = 0;

        console.log(expensesMoved + ' expenses moved');
      }
    }
  } while (expenses.recordset.length > 0);

  await pgConnection.query('ALTER TABLE expenses ALTER COLUMN id RESTART WITH ' + (lastExpenseId + 1));
};

// 38.95

/*

 */