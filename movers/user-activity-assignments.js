const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection, { user_id }) {
  console.log('Moving user-activity assignments...');

  let refsMoved = 0;
  let lastActivityId = 0;
  let refs;

  do {
    refs = await mssqlConnection.query(`
        SELECT *, (SELECT MAX(date_added) FROM tkactivity c WHERE c.activity_id = b.activity_id AND c.timekeeper_id = b.timekeeper_id) date_added  FROM (
        SELECT DISTINCT a.activity_id, a.timekeeper_id FROM tkactivity a
        WHERE a.activity_id IN (
            SELECT TOP 10000 d.activity_id 
            FROM activities d
            WHERE d.activity_id > ${lastActivityId}
             ${user_id === undefined ? '' : `AND d.activity_id IN (SELECT e.activity_id FROM tkactivity e WHERE e.timekeeper_id = ${user_id})`}
            ORDER BY d.activity_id ASC
        )
        AND (SELECT COUNT(*) FROM users f WHERE f.timekeeper_id = a.timekeeper_id) > 0
        ) b ORDER BY b.activity_id ASC`);

    const refsToInsert = [];

    for (let refIndex = 0; refIndex < refs.recordset.length; refIndex++) {
      const ref = refs.recordset[refIndex];

      const dataToInsert = {
        user_id: ref.timekeeper_id,
        activity_id: ref.activity_id,
        created_at: ref.date_added,
      };

      lastActivityId = dataToInsert.activity_id;

      refsToInsert.push(Object.values(dataToInsert));

      if (refsToInsert.length >= 21000 || refIndex === refs.recordset.length - 1) {

        const bulkParams = generateBulkInsert('user_activity_assignments', Object.keys(dataToInsert), refsToInsert);
        await pgConnection.query(bulkParams.sql, bulkParams.params);

        refsMoved += refsToInsert.length;
        refsToInsert.length = 0;

        console.log(refsMoved + ' user-activity assignments moved');
      }
    }
  }
 while (refs.recordset.length > 0);
};