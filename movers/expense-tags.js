const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving expense tags...');

  let expenseTagsMoved = 0;
  let lastTagId;
  const tags = await mssqlConnection.query(`
    SELECT expensePacket.* 
    FROM expensePacket 
    INNER JOIN Users ON Users.timekeeper_id = expensePacket.timekeeper_id
    ORDER BY packetID ASC
    `);
  const tagsToInsert = [];

  for (let tagIndex = 0; tagIndex < tags.recordset.length; tagIndex++) {
    const tag = tags.recordset[tagIndex];

    const dataToInsert = {
      id: tag.packetID,
      name: tag.packetName.trim(),
      description: (tag.desc || '').trim().length > 0 ? (tag.desc || '').trim() : null,
      user_id: tag.Timekeeper_ID,
    };

    lastTagId = dataToInsert.id;

    tagsToInsert.push(Object.values(dataToInsert));

    if (tagsToInsert.length >= 10000 || tagIndex === tags.recordset.length - 1) {
      const bulkParams = generateBulkInsert('expense_tags', Object.keys(dataToInsert), tagsToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      expenseTagsMoved += tagsToInsert.length;
      tagsToInsert.length = 0;

      console.log(expenseTagsMoved + ' expense tags moved');
    }
  }

  await pgConnection.query('ALTER TABLE expense_tags ALTER COLUMN id RESTART WITH ' + (lastTagId + 1));
};