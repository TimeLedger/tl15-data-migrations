const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving activities...');

  let activitiesMoved = 0;
  let lastActivityId;
  const activities = await mssqlConnection.query('SELECT * FROM activities ORDER BY Activity_Id ASC');
  const activitiesToInsert = [];

  for (let activityIndex = 0; activityIndex < activities.recordset.length; activityIndex++) {
    const activity = activities.recordset[activityIndex];

    const dataToInsert = {
      id: activity.Activity_Id,
      name: activity.Activity_name,
      extra_id: activity.Activity_code.trim(),
      extra_id2: activity.Activity_importcode.trim(),
      archived_at: activity.Status === 'A' ? null : new Date(),
      color: (activity.tag_color || '').trim().length > 0 ? activity.tag_color.trim() : null,
      allow_negative_hours: !!activity.allow_negative_hours,
      default_time_entry_billability: activity.activity_billable ? 1 : 0,
      company_id: activity.company_id,
      created_at: activity.date_added || activity.date_modified,
      updated_at: activity.date_modified,
    };

    lastActivityId = dataToInsert.id;

    activitiesToInsert.push(Object.values(dataToInsert));

    if (activitiesToInsert.length >= 5000 || activityIndex === activities.recordset.length - 1) {
      const bulkParams = generateBulkInsert('activities', Object.keys(dataToInsert), activitiesToInsert);
      await pgConnection.query(bulkParams.sql, bulkParams.params);

      activitiesMoved += activitiesToInsert.length;
      activitiesToInsert.length = 0;

      console.log(activitiesMoved + ' activities moved');
    }
  }

  async function afterCommit() {
    await pgConnection.query('ALTER TABLE activities ALTER COLUMN id RESTART WITH ' + (lastActivityId + 1));
  }

  return { afterCommit };
};