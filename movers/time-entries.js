const generateBulkInsert = require('../helpers/generate-bulk-insert');

module.exports = async function(mssqlConnection, pgConnection) {
  console.log('Moving time entries...');

  let entriesMoved = 0;
  let lastEntryId = 0;
  let entries;

  do {
    entries = await mssqlConnection.query(`
        SELECT TOP 200000 * FROM Entries 
        WHERE 
        entry_id > ${lastEntryId}
        AND (
            SELECT COUNT(*) FROM Projects 
            WHERE Projects.Project_Id = Entries.Project_id AND
            (SELECT COUNT(*) FROM Clients WHERE Projects.Client_id = Clients.Client_Id) > 0
        ) > 0
        AND (
            SELECT COUNT(*) FROM activities 
            WHERE activities.Activity_Id = Entries.activity 
        ) > 0
        ORDER BY entry_id ASC
        `);

    const entriesToInsert = [];
    const changesToInsert = [];

    for (let entryIndex = 0; entryIndex < entries.recordset.length; entryIndex++) {
      const entry = entries.recordset[entryIndex];

      const dataToInsert = {
        id: entry.entry_id,
        time: entry.Bill_time,
        date: entry.Start_Date,
        billable: entry.billable,
        activity_id: !entry.activity || entry.activity === 1 ? null : entry.activity,
        user_id: entry.user_id,
        project_id: entry.Project_id,
        created_at: entry.Start_Time,
        description: (entry.Description || '').replace(/\0/g, '').trim(),
      };

      entriesToInsert.push(Object.values(dataToInsert));

      lastEntryId = dataToInsert.id;

      const change = {
        time_entry_id: dataToInsert.id,
        date: null,
        time: null,
        action: 'create',
        actor_id: dataToInsert.user_id,
        billable: null,
        description: null,
        activity_id: null,
        project_id: null,
        created_at: null,
      };

      const changeCreated = Object.assign({}, change, {
        date: dataToInsert.date,
        time: dataToInsert.time,
        billable: dataToInsert.billable,
        description: dataToInsert.description,
        activity_id: dataToInsert.activity_id,
        project_id: dataToInsert.project_id,
        action: 'create',
        actor_id: dataToInsert.user_id,
        created_at: dataToInsert.created_at,
      });

      if (!!entry.modified_by) {
        const changeUpdated = Object.assign({}, change, {
          action: 'update',
          actor_id: entry.modified_by,
          created_at: null,
        });

        changesToInsert.push(Object.values(changeCreated));
        changesToInsert.push(Object.values(changeUpdated));
      }

      if (entriesToInsert.length >= 5000 || entryIndex === entries.recordset.length - 1) {
        let bulkParams = generateBulkInsert('time_entries', Object.keys(dataToInsert), entriesToInsert);
        await pgConnection.query(bulkParams.sql, bulkParams.params);

        if (changesToInsert.length > 0) {
          bulkParams = generateBulkInsert('time_entry_changes', Object.keys(change), changesToInsert);
          await pgConnection.query(bulkParams.sql, bulkParams.params);
        }

        entriesMoved += entriesToInsert.length;
        entriesToInsert.length = 0;
        changesToInsert.length = 0;

        console.log(entriesMoved + ' time entries moved');
      }
    }
  } while (entries.recordset.length > 0);

  await pgConnection.query('ALTER TABLE time_entries ALTER COLUMN id RESTART WITH ' + (lastEntryId + 1));
};