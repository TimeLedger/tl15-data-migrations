const mssql = require('mssql');
const pg = require('pg');

const moveUsers = require('./movers/users');
const moveCompanies = require('./movers/companies');
const moveClients = require('./movers/clients');
const moveClientContacts = require('./movers/client-contacts');
const moveProjects = require('./movers/projects');
const moveProjectCategories = require('./movers/project-categories');
const moveActivities = require('./movers/activities');
const moveTimeEntries = require('./movers/time-entries');
const moveExpenseTypes = require('./movers/expense-types');
const moveExpenseTags = require('./movers/expense-tags');
const moveExpenses = require('./movers/expenses');
const moveUserProjectAssignments = require('./movers/user-project-assignments');
const moveUserActivityAssignments = require('./movers/user-activity-assignments');
const moveTimesheetItems = require('./movers/timesheet-items');
const moveTeams = require('./movers/teams');
const moveUserTeamAssignments = require('./movers/user-team-assignments');

async function getMssqlConnection() {
  return new mssql.ConnectionPool({
    server: 'localhost',
    user: 'sa',
    password: 'YEYfcQ2mJVSJKjGw',
    port: 1433,
    database: 'oldtl11112020',
    options: {
      useUTC: true,
      enableArithAbort: true,
    },
    requestTimeout: 60000,
  }).connect();
}

function getPgConnection() {
  return (new pg.Pool({
    user: 'dev',
    host: 'localhost',
    database: 'dev',
    password: 'dev',
    pool: {
      min: 5,
    },
    port: 5432,
  })).connect();
}

(async function() {
  const mssqlConnection = await getMssqlConnection();
  const pgConnection = await getPgConnection();

  await pgConnection.query('TRUNCATE time_entries CASCADE');
  await pgConnection.query('TRUNCATE user_project_assignments CASCADE');
  await pgConnection.query('TRUNCATE user_activity_assignments CASCADE');
  await pgConnection.query('TRUNCATE clients CASCADE');
  await pgConnection.query('TRUNCATE client_contacts CASCADE');
  await pgConnection.query('TRUNCATE activities CASCADE');
  await pgConnection.query('TRUNCATE companies CASCADE');
  await pgConnection.query('TRUNCATE project_categories CASCADE');
  await pgConnection.query('TRUNCATE projects CASCADE');
  await pgConnection.query('TRUNCATE time_entry_changes CASCADE');
  await pgConnection.query('TRUNCATE users CASCADE');
  await pgConnection.query('TRUNCATE timesheet_items CASCADE');
  await pgConnection.query('TRUNCATE expense_types CASCADE');
  await pgConnection.query('TRUNCATE expense_tags CASCADE');
  await pgConnection.query('TRUNCATE expenses CASCADE');
  await pgConnection.query('TRUNCATE expense_changes CASCADE');
  await pgConnection.query('TRUNCATE teams CASCADE');

  try {
    await pgConnection.query('START TRANSACTION');
    await pgConnection.query('SET CONSTRAINTS ALL DEFERRED');

    // Move companies
    const companies = await moveCompanies(mssqlConnection, pgConnection);

    // Move activities
    const activities = await moveActivities(mssqlConnection, pgConnection);

    // Move users
    const users = await moveUsers(mssqlConnection, pgConnection);

    // Move client contacts
    const clientContacts = await moveClientContacts(mssqlConnection, pgConnection);

    // Move clients
    const clients = await moveClients(mssqlConnection, pgConnection);

    await pgConnection.query('COMMIT');

    await companies.afterCommit();
    await activities.afterCommit();
    await users.afterCommit();
    await clients.afterCommit();
    await clientContacts.afterCommit();
  } catch (e) {
    await pgConnection.query('ROLLBACK');
    throw e;
  }

  // Move teams
  await moveTeams(mssqlConnection, pgConnection);

  // Move user-team assignments
  await moveUserTeamAssignments(mssqlConnection, pgConnection);

  // Move user-activity assignments
  await moveUserActivityAssignments(mssqlConnection, pgConnection, {});

  // Move project categories
  await moveProjectCategories(mssqlConnection, pgConnection);

  // Move projects
  await moveProjects(mssqlConnection, pgConnection);

  // Move user-project assignments
  await moveUserProjectAssignments(mssqlConnection, pgConnection, {});

  // Move expense types
  await moveExpenseTypes(mssqlConnection, pgConnection);

  // Move expense tags
  await moveExpenseTags(mssqlConnection, pgConnection);

  // Move expenses
  await moveExpenses(mssqlConnection, pgConnection, {});

  // Move timesheet items
  await moveTimesheetItems(mssqlConnection, pgConnection);

  // Move time entries
  await moveTimeEntries(mssqlConnection, pgConnection);
})().catch(console.error).finally(() => {
  process.exit();
});