module.exports = function generateBulkInsert(tableName, columns, rows) {
  const values = [];
  const params = [];

  for (let rowIndex = 0; rowIndex < rows.length; rowIndex++) {
    const row = rows[rowIndex];
    const items = [];

    for (let valueIndex = 0; valueIndex < row.length; valueIndex++) {
      const value = row[valueIndex];
      params.push(value);
      items.push('$' + (params.length));
    }

    values.push(items);
  }

  const sql = 'INSERT INTO ' + tableName + ' (' + columns.join(', ') + ') OVERRIDING SYSTEM VALUE VALUES ' + values.map(items => '(' + items.join(', ') + ')').join(', ');

  return {
    sql,
    params,
  };
};