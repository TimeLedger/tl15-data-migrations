const bcrypt = require('bcrypt');

module.exports = async function hashPassword(plainText, cost) {
  return bcrypt.hash(plainText, cost);
};